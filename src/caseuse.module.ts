import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserCase } from './application/case-use/user.case';
import { UserController } from './infraestructure/controller/user.controller';
import * as dotenv from 'dotenv';
import { UserSchema } from './domain/schemas/user.schema';
dotenv.config();


@Module({
    imports: [
        MongooseModule.forRoot(process.env.DB),
        MongooseModule.forFeature([
            {
              name: 'User',
              schema: UserSchema,
            },
          ]),
      ],
    providers: [ UserCase ],  
    controllers: [UserController]  
})
export class CaseuseModule {}
