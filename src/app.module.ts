import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import * as dotenv from 'dotenv';
import { CaseuseModule } from './caseuse.module';
dotenv.config();

@Module({
  imports: [CaseuseModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
