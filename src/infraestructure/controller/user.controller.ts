import { Controller, HttpStatus } from '@nestjs/common';
import { Body, Get, Param, Post, Res, Response } from '@nestjs/common/decorators';
import { UserCase } from 'src/application/case-use/user.case';
import { LoginDTO } from 'src/domain/dto/login.dto';
import { UserDTO } from 'src/domain/dto/user.dto';

@Controller('user')
export class UserController {
    constructor(private readonly userCase: UserCase) {}
    
    @Post('/')
    async register(@Body() userDTO: UserDTO): Promise<any> {

        try {

            const user = await this.userCase.newUser(userDTO);

            return user;
            
        } catch (err) {
            console.log(err);
        }

    }

    @Post('/token')
    async login(@Res() res, @Body() loginDTO: LoginDTO): Promise<any> {

        try {

            const token = await this.userCase.authentication(loginDTO);

            res.status(HttpStatus.OK).json({token : token});
            
        } catch (err) {
            console.log(err);
            
        }

    }
}
