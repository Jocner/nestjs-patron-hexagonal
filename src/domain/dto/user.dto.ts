import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';


export class UserDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    readonly name: string;
  
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    readonly email: string;
  
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    readonly password: string;
  
    @ApiProperty()
    @IsString()
    readonly image: string;
  }

  // export class LoginDTO {
  //   @ApiProperty()
  //   @IsNotEmpty()
  //   @IsString()
  //   readonly email: string;
  
  //   @ApiProperty()
  //   @IsNotEmpty()
  //   @IsString()
  //   readonly password: string;

  // }  