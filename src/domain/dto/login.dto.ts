import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class LoginDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    readonly email: string;
  
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    readonly password: string;

  }  