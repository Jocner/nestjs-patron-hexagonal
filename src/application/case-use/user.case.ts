import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDTO } from 'src/domain/dto/user.dto';
import { UserDocument } from 'src/domain/schemas/user.schema';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { LoginDTO } from 'src/domain/dto/login.dto';



@Injectable()
export class UserCase {
    constructor(
        @InjectModel('User')
        private UserModel: Model<UserDocument>,
    ) {}

    async newUser(user : UserDTO):Promise<any>{
        try {
            
            if(!user) return 

            const password = user.password;

            const round = 10;
            const passEncrip = await bcrypt.hash(password, round);
 
            // const register = this.UserModel.create(user);
            const register = await this.UserModel.create({ name : user.name, email : user.email, password : passEncrip, image : user.image});
            
            return register;

        } catch(err) {
            console.log(err);
        }
    }

    async authentication(user : LoginDTO): Promise<any>{
        try {

            if(!user) return
            console.log('case', user);
            const data = await this.UserModel.findOne({email : user.email})
            console.log('CONSULTA DB', data)

            const token = jwt.sign({
                data: data
            }, process.env.SECRET, {
                expiresIn: 60 * 60 * 24
            }
            )

            return token;
            
        } catch (err) {
            console.log(err)
            
        }
    }

}
